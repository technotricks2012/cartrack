package com.kishore.cartrack.ui.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth
import com.kishore.cartrack.MainCoroutineRule
import com.kishore.cartrack.data.repositories.FakeUserRepository
import com.kishore.cartrack.data.repositories.IUserRepository
import com.kishore.cartrack.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class HomeViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: HomeViewModel

    private lateinit var userRepo: IUserRepository

    @Before
    fun setUp() {
        userRepo = FakeUserRepository()
        viewModel = HomeViewModel(userRepo)
    }

    @Test
    fun getUserDetails() = runBlockingTest {
        val result = viewModel.getUser().getOrAwaitValue()
        Truth.assertThat(result.size).isEqualTo(1)
    }
}