package com.kishore.cartrack.ui.login

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.core.app.ApplicationProvider
import com.google.common.truth.Truth
import com.kishore.cartrack.MainCoroutineRule
import com.kishore.cartrack.data.db.entities.User
import com.kishore.cartrack.data.file.FileManager
import com.kishore.cartrack.data.repositories.CountryRepository
import com.kishore.cartrack.data.repositories.FakeAuthRepository
import com.kishore.cartrack.data.repositories.IAuthRepository
import com.kishore.cartrack.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test


@ExperimentalCoroutinesApi
class LoginViewModelTest {


    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: LoginViewModel

    private lateinit var authRepo: IAuthRepository

    @Before
    fun setUp() {
        authRepo = FakeAuthRepository()
        val fileManager = FileManager( ApplicationProvider.getApplicationContext())
        val countryRepository = CountryRepository(fileManager)

        viewModel = LoginViewModel(authRepo,countryRepository)


    }

    @Test
    fun onLoginSuccess() = runBlockingTest {
        viewModel.email = "kishore@gmail.com"
        viewModel.password = "123456"

        authRepo.signUpUser(User(email = viewModel.email, password = viewModel.password))

        viewModel.onLoginClick(null)

        val result = viewModel.getSuccess().getOrAwaitValue()
        Truth.assertThat(result).isEqualTo("Success Login")
    }


    @Test
    fun invalidCredentials() = runBlockingTest {
        viewModel.email = "kishore@gmail.com"
        viewModel.password = "123456"

        authRepo.signUpUser(User(email = viewModel.email, password = "password"))

        viewModel.onLoginClick(null)

        val result = viewModel.getError().getOrAwaitValue()
        Truth.assertThat(result).isEqualTo("Invalid credentials")
    }


    @Test
    fun emailFiledValidation() = runBlockingTest {
        viewModel.email = ""
        viewModel.password = "123456"

        viewModel.onLoginClick(null)

        val result = viewModel.getError().getOrAwaitValue()
        Truth.assertThat(result).isEqualTo("Email should not be empty")
    }

    @Test
    fun emailValidation() = runBlockingTest {
        viewModel.email = "kishore"
        viewModel.password = "123456"

        viewModel.onLoginClick(null)

        val result = viewModel.getError().getOrAwaitValue()
        Truth.assertThat(result).isEqualTo("Invalid email")
    }

    @Test
    fun passwordEmptyValidation() = runBlockingTest {
        viewModel.email = "kishore@gmail.com"
        viewModel.password = ""

        viewModel.onLoginClick(null)

        val result = viewModel.getError().getOrAwaitValue()
        Truth.assertThat(result).isEqualTo("Please enter the password")
    }

    @Test
    fun passwordValidation() = runBlockingTest {
        viewModel.email = "kishore@gmail.com"
        viewModel.password = "123"

        viewModel.onLoginClick(null)

        val result = viewModel.getError().getOrAwaitValue()
        Truth.assertThat(result).isEqualTo("Password should be minimum 5 character")
    }

}