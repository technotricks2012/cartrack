package com.kishore.cartrack.ui.signup

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth
import com.kishore.cartrack.MainCoroutineRule
import com.kishore.cartrack.data.db.entities.User
import com.kishore.cartrack.data.repositories.FakeAuthRepository
import com.kishore.cartrack.data.repositories.IAuthRepository
import com.kishore.cartrack.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test


@ExperimentalCoroutinesApi
class SignUpViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: SignUpViewModel

    private lateinit var authRepo: IAuthRepository

    @Before
    fun setUp() {
        authRepo = FakeAuthRepository()
        viewModel = SignUpViewModel(authRepo)

    }

    @Test
    fun onSignUpSuccess() = runBlockingTest {
        viewModel.email = "kishore@gmail.com"
        viewModel.password = "123456"
        viewModel.cPassword = "123456"

        authRepo.signUpUser(User(email = viewModel.email, password = viewModel.password))

        viewModel.onSignUpClick(null)

        val result = viewModel.getSuccess().getOrAwaitValue()
        Truth.assertThat(result).isEqualTo("Registration Success")
    }



    @Test
    fun emailFiledValidation() = runBlockingTest {
        viewModel.email = ""
        viewModel.password = "123456"

        viewModel.onSignUpClick(null)

        val result = viewModel.getError().getOrAwaitValue()
        Truth.assertThat(result).isEqualTo("Email should not be empty")
    }

    @Test
    fun emailValidation() = runBlockingTest {
        viewModel.email = "kishore"
        viewModel.password = "123456"

        viewModel.onSignUpClick(null)

        val result = viewModel.getError().getOrAwaitValue()
        Truth.assertThat(result).isEqualTo("Invalid email")
    }

    @Test
    fun passwordEmptyValidation() = runBlockingTest {
        viewModel.email = "kishore@gmail.com"
        viewModel.password = ""

        viewModel.onSignUpClick(null)

        val result = viewModel.getError().getOrAwaitValue()
        Truth.assertThat(result).isEqualTo("Please enter the password")
    }

    @Test
    fun passwordValidation() = runBlockingTest {
        viewModel.email = "kishore@gmail.com"
        viewModel.password = "123"

        viewModel.onSignUpClick(null)

        val result = viewModel.getError().getOrAwaitValue()
        Truth.assertThat(result).isEqualTo("Password should be minimum 5 character")
    }


    @Test
    fun cpasswordEmptyValidation() = runBlockingTest {
        viewModel.email = "kishore@gmail.com"
        viewModel.password = "123456"
        viewModel.cPassword = ""

        viewModel.onSignUpClick(null)

        val result = viewModel.getError().getOrAwaitValue()
        Truth.assertThat(result).isEqualTo("Please enter the confirm password")
    }

    @Test
    fun passwordMismatch() = runBlockingTest {
        viewModel.email = "kishore@gmail.com"
        viewModel.password = "12345"
        viewModel.cPassword = "123456"

        viewModel.onSignUpClick(null)

        val result = viewModel.getError().getOrAwaitValue()
        Truth.assertThat(result).isEqualTo("Password Mismatch")
    }

}