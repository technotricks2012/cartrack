package com.kishore.cartrack.utils

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth.assertThat
import junit.framework.TestCase
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class StringExtKtTest : TestCase(){


    @Test
    fun checkValidEmail(){
        val result = "kishore@gmail.com".isValidEmail()
        assertThat(result).isEqualTo(true)
    }

    @Test
    fun checkNegativeValidEmail(){
        val result = "kishore".isValidEmail()
        assertThat(result).isEqualTo(false)
    }


}