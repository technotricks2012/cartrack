package com.kishore.cartrack.data.network

import com.kishore.cartrack.data.network.response.UserListResponse
import retrofit2.Response


class FakeMyApi:Api {
    private var response= UserListResponse(name = "FakeMyApi")
    override suspend fun getUsers(): Response<List<UserListResponse>>
            = Response.success(listOf(response))

}