package com.kishore.cartrack.data.repositories

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.kishore.cartrack.data.network.FakeMyApi
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@SmallTest
class UserRepositoryTest: TestCase() {

    private lateinit var repo: IUserRepository

    @Before
    public override fun setUp() {
        repo = UserRepository(FakeMyApi())
    }

    @Test
    fun insertUser()= runBlocking {
        val result= repo.getUsers()

        assertTrue(result.size==1)

    }
}