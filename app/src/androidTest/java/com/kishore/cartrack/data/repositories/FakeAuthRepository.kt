package com.kishore.cartrack.data.repositories

import com.kishore.cartrack.data.db.entities.User


class FakeAuthRepository :IAuthRepository{

    private var _user: User?=null
    override suspend fun signUpUser(user: User): Long {
        _user = user
        return 1
    }

    override suspend fun getUser(email: String, password: String): User? {
        return if(email==_user?.email && password == _user?.password)_user
        else
            null
    }
}