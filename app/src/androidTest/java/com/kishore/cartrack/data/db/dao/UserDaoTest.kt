package com.kishore.cartrack.data.db.dao

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.kishore.cartrack.data.db.AppDatabase
import com.kishore.cartrack.data.db.entities.User
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@SmallTest
class UserDaoTest : TestCase() {

    private lateinit var db: AppDatabase
    private lateinit var dao:UserDao

    @Before
    public override fun setUp() {
        db = Room.inMemoryDatabaseBuilder(ApplicationProvider.getApplicationContext(), AppDatabase::class.java)
            .allowMainThreadQueries()
            .build()
        dao = db.getUserDao()
    }
    @After
    public override fun tearDown() {
        db.close()
    }

    @Test
    fun insertUser()= runBlocking {
        val user = User(email = "kishore@gmail.com", password = "12345")

        dao.insert(user)
        val result = dao.getUser(email = "kishore@gmail.com", password = "12345")
        assertTrue(user.email == result.email)
    }
}