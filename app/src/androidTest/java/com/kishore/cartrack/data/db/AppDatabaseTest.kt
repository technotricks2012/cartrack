package com.kishore.cartrack.data.db

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.kishore.cartrack.data.db.dao.UserDao
import com.kishore.cartrack.data.db.entities.User
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AppDatabaseTest : TestCase() {

    private lateinit var db: AppDatabase
    private lateinit var dao: UserDao


    @Before
    public override fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        dao = db.getUserDao()
    }

    @Test
    fun insertAndQueryUser() = runBlocking {

        val user = User(email = "kishore@gmail.com", password = "12345")

        dao.insert(user)
        val result = dao.getUser(email = "kishore@gmail.com", password = "12345")
        assertTrue(user.email == result.email)
    }
}