package com.kishore.cartrack.data.repositories

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.kishore.cartrack.data.db.AppDatabase
import com.kishore.cartrack.data.db.entities.User
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class AuthRepositoryTest:TestCase() {

    private lateinit var db: AppDatabase
    private lateinit var repo:IAuthRepository

    @Before
    public override fun setUp() {
        db = Room.inMemoryDatabaseBuilder(
                ApplicationProvider.getApplicationContext(),
                AppDatabase::class.java)
            .allowMainThreadQueries()
            .build()
        repo= AuthRepository(db)
    }

    @After
    public override fun tearDown() {
        db.close()
    }

    @Test
    fun insertUser()= runBlocking {
        val user = User(email = "kishore@gmail.com", password = "12345")

        repo.signUpUser(user)
        val result = repo.getUser(email = user.email!!,password = user.password!!)
        assertTrue(user.email == result?.email)
    }
}