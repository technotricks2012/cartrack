package com.kishore.cartrack.data.repositories

import com.kishore.cartrack.data.network.response.UserListResponse


class FakeUserRepository :IUserRepository{
    override suspend fun getUsers(): List<UserListResponse>
            = listOf(UserListResponse(email = "kishore@gmail.com"))

}