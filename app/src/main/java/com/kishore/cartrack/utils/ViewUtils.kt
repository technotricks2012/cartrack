package com.kishore.cartrack.utils

import android.graphics.Color
import android.view.View
import com.google.android.material.snackbar.Snackbar


fun View.snackbar(message: String, isError: Boolean = false) {
    Snackbar.make(this, message, Snackbar.LENGTH_LONG).also { snackbar ->
        snackbar.setAction("OK") {
            snackbar.dismiss()
        }
        snackbar.view.setBackgroundColor(Color.parseColor(if (isError) "#FFBA1111" else "#FF43A047"))

    }.show()
}

fun View.show(){
    this.visibility = View.VISIBLE
}

fun View.hide(){
    this.visibility = View.INVISIBLE
}