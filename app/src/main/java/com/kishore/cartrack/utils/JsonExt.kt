package com.kishore.cartrack.utils

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.IOException
import java.io.InputStream


class LocalJSONParser {
    companion object {
        fun inputStreamToString(inputStream: InputStream): String {
            try {
                val bytes = ByteArray(inputStream.available())
                inputStream.read(bytes, 0, bytes.size)
                return String(bytes)
            } catch (e: IOException) {
                return ""
            }
        }
    }
}

inline fun <reified T> Context.getObjectFromJson(jsonFileName: String): T {
    val myJson =LocalJSONParser.inputStreamToString(this.assets.open(jsonFileName))
    return Gson().fromJson(myJson, T::class.java)
}


inline fun <reified T> Context.getObjectFromJsonList(jsonFileName: String): List<T>? {
    val myJson =LocalJSONParser.inputStreamToString(this.assets.open(jsonFileName))
    val typeToken = TypeToken.getParameterized(List::class.java, T::class.java)
    return Gson().fromJson<List<T>>(myJson, typeToken.type)
}