package com.kishore.cartrack.data.network

import com.kishore.cartrack.data.network.response.UserListResponse
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface Api {

    @GET("users")
    suspend fun getUsers(): Response<List<UserListResponse>>


    companion object{
        operator fun invoke(networkConnectionInterceptor:NetworkConnectionInterceptor):Api{

            val okkHttpClient = OkHttpClient.Builder()
                .addInterceptor(networkConnectionInterceptor)
                .build()
            return Retrofit.Builder()
                .client(okkHttpClient)
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(Api::class.java)
        }
    }
}