package com.kishore.cartrack.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kishore.cartrack.data.db.entities.User

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(user: User):Long

    @Query(value = "Select * From user Where email= :email AND password = :password")
    suspend fun getUser(email:String,password:String): User
}