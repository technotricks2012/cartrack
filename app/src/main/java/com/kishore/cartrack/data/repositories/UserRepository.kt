package com.kishore.cartrack.data.repositories

import com.kishore.cartrack.data.network.Api
import com.kishore.cartrack.data.network.SafeApiRequest
import com.kishore.cartrack.data.network.response.UserListResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


interface   IUserRepository{
    suspend fun getUsers():List<UserListResponse>
}

class UserRepository(private val api: Api) : SafeApiRequest(),IUserRepository{

    override suspend fun getUsers(): List<UserListResponse> {
        return withContext(Dispatchers.IO) {
            apiRequest {
                api.getUsers()
            }
        }
    }
}