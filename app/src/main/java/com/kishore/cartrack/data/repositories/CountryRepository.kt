package com.kishore.cartrack.data.repositories

import com.kishore.cartrack.data.file.IFileManager
import com.kishore.cartrack.data.file.model.Country
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


interface   ICountryRepository{
    suspend fun getCountries():List<Country>?
}


class CountryRepository(private val fileManager: IFileManager):ICountryRepository {
    override suspend fun getCountries(): List<Country>? =
        withContext(Dispatchers.IO) { fileManager.getCountries() }


}