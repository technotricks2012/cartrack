package com.kishore.cartrack.data.repositories

import com.kishore.cartrack.data.db.AppDatabase
import com.kishore.cartrack.data.db.entities.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


interface   IAuthRepository{
    suspend fun signUpUser(user: User):Long
    suspend fun getUser(email: String, password: String): User?
}

class AuthRepository(private val db: AppDatabase):IAuthRepository {

    override suspend fun signUpUser(user: User) =
        withContext(Dispatchers.IO) { db.getUserDao().insert(user) }

    override suspend fun getUser(email: String, password: String) =
        withContext(Dispatchers.IO) { db.getUserDao().getUser(email, password) }
}

