package com.kishore.cartrack.data.file

import android.content.Context
import com.kishore.cartrack.data.file.model.Country
import com.kishore.cartrack.utils.getObjectFromJsonList


interface   IFileManager{
    suspend fun getCountries():List<Country>?
}


class FileManager(private val context: Context):IFileManager{
    override suspend fun getCountries() =
        context.getObjectFromJsonList<Country>("countries.json")

}