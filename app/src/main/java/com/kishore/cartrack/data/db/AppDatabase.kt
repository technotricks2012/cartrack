package com.kishore.cartrack.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.kishore.cartrack.data.db.dao.UserDao
import com.kishore.cartrack.data.db.entities.User


@Database(
    entities = [User::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase: RoomDatabase() {

    abstract fun getUserDao(): UserDao
    companion object{
        @Volatile
        private var INSTANCE:AppDatabase?=null

        private val LOCK = Any()
        operator  fun invoke(context: Context) = INSTANCE?: synchronized(LOCK){
            INSTANCE?:buildDatabase(context).also {
                INSTANCE = it
            }
        }

        private fun buildDatabase(context: Context)=
            Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                "CarDatabase.db"
            ).build()
    }
}