package com.kishore.cartrack.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
@Entity
data class User(
    @PrimaryKey
    var id:Int?=0,
    var email:String?=null,
    var password:String?=null
)