package com.kishore.cartrack.data.file.model

data class Country(
    var name: String? = null,
    var code: String? = null
)