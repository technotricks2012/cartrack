package com.kishore.cartrack

import android.app.Application
import com.kishore.cartrack.data.db.AppDatabase
import com.kishore.cartrack.data.file.FileManager
import com.kishore.cartrack.data.network.Api
import com.kishore.cartrack.data.network.NetworkConnectionInterceptor
import com.kishore.cartrack.data.repositories.AuthRepository
import com.kishore.cartrack.data.repositories.CountryRepository
import com.kishore.cartrack.data.repositories.UserRepository
import com.kishore.cartrack.ui.home.HomeViewModelFactory
import com.kishore.cartrack.ui.login.LoginViewModelFactory
import com.kishore.cartrack.ui.signup.SignUpViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class CarTrackApplication : Application(), KodeinAware {
    override val kodein = Kodein.lazy {
        import(
            androidXModule(this@CarTrackApplication)
        )
        bind() from singleton { AppDatabase(instance()) }
        bind() from singleton { AuthRepository(instance()) }

        bind() from singleton { FileManager(instance()) }
        bind() from singleton { CountryRepository(instance()) }

        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { Api(instance()) }
        bind() from singleton { UserRepository(instance()) }

        bind() from provider { SignUpViewModelFactory(instance()) }
        bind() from provider { LoginViewModelFactory(instance(), instance()) }

        bind() from provider { HomeViewModelFactory(instance()) }

    }
}