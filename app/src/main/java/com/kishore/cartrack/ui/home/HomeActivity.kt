package com.kishore.cartrack.ui.home

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.kishore.cartrack.R
import com.kishore.cartrack.data.network.response.Geo
import com.kishore.cartrack.data.network.response.UserListResponse
import com.kishore.cartrack.databinding.ActivityHomeBinding
import com.kishore.cartrack.utils.hide
import com.kishore.cartrack.utils.show
import com.kishore.cartrack.utils.snackbar
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_home.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import kotlin.math.absoluteValue

class HomeActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory:HomeViewModelFactory by instance()

    private lateinit var gMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityHomeBinding = DataBindingUtil.setContentView(this, R.layout.activity_home)
        val viewModel = ViewModelProvider(this,factory).get(HomeViewModel::class.java)
        binding.viewmodel = viewModel

        actionBar?.show()
        viewModel.isLoading().observe(this,{
            it?.let {
                when(it){
                    true -> animation_viewLoading.show()
                    else ->animation_viewLoading.hide()
                }
            }
        })

        viewModel.getUser().observe(this,  {
            initRecyclerView(it.toUserItem())
            showMapView(it)
        })

        viewModel.getError().observe(this,  {
            rootLayout.snackbar(it,true)
        })
    }

    private fun initRecyclerView(userItems: List<UserItem>?=null,gotoPosition:Int?=null) {
        if(gotoPosition!=null){
            recyclerView.scrollToPosition(gotoPosition)
            return
        }
        userItems?.let {
            val mAdapter = GroupAdapter<ViewHolder>().apply {
                addAll(userItems)
            }
            recyclerView.apply {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false)

                setHasFixedSize(true)
                adapter = mAdapter
            }
            mAdapter.setOnItemClickListener { item, view ->
                it.get(0).userListResponse
            }
            mAdapter.setOnItemClickListener { item, view ->
                showMapView(gotoPosition=it.getOrNull((item.id.absoluteValue).toInt()-1)?.userListResponse)
            }
        }
    }

    private fun showMapView(userItems: List<UserListResponse>?=null, gotoPosition: UserListResponse?=null) {
        if(gotoPosition!=null){
            gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(gotoPosition.address?.geo?.toLatLong(), 1F))
            return
        }

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
        mapFragment?.getMapAsync { googleMap ->
            gMap = googleMap
            MapsInitializer.initialize(this)
            userItems?.forEach {
                it.address?.geo?.toLatLong()?.let {location->

                    googleMap.addMarker(MarkerOptions().position(location).title(it.name))
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 1F))
                }
            }

            googleMap.setOnMarkerClickListener {
                it.showInfoWindow()
                initRecyclerView(gotoPosition = it.id.drop(1).toInt())
                return@setOnMarkerClickListener true
            }
        }
    }

    private fun Geo.toLatLong(): LatLng = LatLng(this.lat?.toDouble()?:0.0,this.lng?.toDouble()?:0.0)

    private fun List<UserListResponse>.toUserItem():List<UserItem>{
        return this.map {
            UserItem(it)
        }
    }

    companion object {
        fun getStartIntent(
            context: Context
        ) = Intent(context, HomeActivity::class.java)
    }
}