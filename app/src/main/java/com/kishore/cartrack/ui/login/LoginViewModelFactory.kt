package com.kishore.cartrack.ui.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kishore.cartrack.data.repositories.IAuthRepository
import com.kishore.cartrack.data.repositories.ICountryRepository


@Suppress("UNCHECKED_CAST")
class LoginViewModelFactory(
    private val repository: IAuthRepository,
    private val countryRepository: ICountryRepository
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LoginViewModel(repository, countryRepository) as T
    }
}