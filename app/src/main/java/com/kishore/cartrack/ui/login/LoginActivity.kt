package com.kishore.cartrack.ui.login

import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.kishore.cartrack.R
import com.kishore.cartrack.data.file.model.Country
import com.kishore.cartrack.databinding.ActivityLoginBinding
import com.kishore.cartrack.ui.home.HomeActivity
import com.kishore.cartrack.ui.signup.SignUpActivity
import com.kishore.cartrack.utils.hide
import com.kishore.cartrack.utils.show
import com.kishore.cartrack.utils.snackbar
import kotlinx.android.synthetic.main.activity_login.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class LoginActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: LoginViewModelFactory by instance()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityLoginBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_login
        )
        val viewModel = ViewModelProvider(this, factory).get(LoginViewModel::class.java)
        binding.viewmodel = viewModel

        tvSignUp.setOnClickListener {
            SignUpActivity.getStartIntent(this).also {
                startActivity(it)
            }
        }
        viewModel.isLoading().observe(this, {
            it?.let {
                when (it) {
                    true -> progressBar.show()
                    else ->progressBar.hide()
                }
            }
        })

        viewModel.getError().observe(this, {
            it.let {
                rootLayout.snackbar(it, true)
            }
        })


        menuCountry.setOnClickListener {
            menuCountry.showDropDown()
        }

        viewModel.getSuccess().observe(this, {
            it.let {
                HomeActivity.getStartIntent(this).also {
                    startActivity(it)
                    finish()
                }
            }
        })

        viewModel.listCountries.observe(this, {
            it?.let {
                initCountryView(it.toCountry())
            }

        })

    }

    private fun List<Country>.toCountry(): List<String> {
        return this.map {
            it.name ?: ""
        }
    }

    private fun initCountryView(countryItems: List<String>) {
        val adapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, countryItems)
        menuCountry?.setAdapter(adapter)
    }

}