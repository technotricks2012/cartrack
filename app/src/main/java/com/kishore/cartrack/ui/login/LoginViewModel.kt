package com.kishore.cartrack.ui.login

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kishore.cartrack.data.file.model.Country
import com.kishore.cartrack.data.repositories.IAuthRepository
import com.kishore.cartrack.data.repositories.ICountryRepository
import com.kishore.cartrack.utils.Coroutines
import com.kishore.cartrack.utils.isValidEmail

class LoginViewModel(
    private val repository: IAuthRepository,
    private val countryRepository: ICountryRepository
) : ViewModel() {

    var email: String? = null
    var password: String? = null
    var selectedCountry: String? = null

    private val _isLoading: MutableLiveData<Boolean> = MutableLiveData()
    fun isLoading(): LiveData<Boolean> = _isLoading

    private val _isErrorMessage: MutableLiveData<String> = MutableLiveData()
    fun getError(): LiveData<String> = _isErrorMessage

    private val _isSuccessMessage: MutableLiveData<String> = MutableLiveData()
    fun getSuccess(): LiveData<String> = _isSuccessMessage


    private val _listCountries: MutableLiveData<List<Country>> = MutableLiveData()
    val listCountries: LiveData<List<Country>> = _listCountries

    init {
        onCountyCall()
    }

    fun onLoginClick(view: View?) {
        _isLoading.value = true
        when {
            email.isNullOrBlank() -> {
                _isLoading.value = false
                _isErrorMessage.value = "Email should not be empty"
                return
            }
            email?.isValidEmail() == false -> {
                _isLoading.value = false
                _isErrorMessage.value = "Invalid email"
                return
            }
            password.isNullOrBlank() -> {
                _isLoading.value = false
                _isErrorMessage.value = "Please enter the password"
                return
            }
            password?.length ?: 0 < 5 -> {
                _isLoading.value = false
                _isErrorMessage.value = "Password should be minimum 5 character"
                return
            }
            else -> Coroutines.main {
                val response = repository.getUser(email!!, password!!)
                _isLoading.value = false
                if (response == null) {
                    _isErrorMessage.value = "Invalid credentials"
                    return@main
                }
                _isSuccessMessage.value = "Success Login"
            }
        }
    }

    private fun onCountyCall() {
        Coroutines.main {
            _listCountries.value = countryRepository.getCountries()
        }
    }
}