package com.kishore.cartrack.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kishore.cartrack.data.network.response.UserListResponse
import com.kishore.cartrack.data.repositories.IUserRepository
import com.kishore.cartrack.utils.ApiException
import com.kishore.cartrack.utils.Coroutines
import com.kishore.cartrack.utils.NoInternetException

class HomeViewModel(private val repository: IUserRepository): ViewModel() {


    private val _isLoading: MutableLiveData<Boolean> = MutableLiveData()
    fun isLoading(): LiveData<Boolean> = _isLoading

    private val _isErrorMessage: MutableLiveData<String> = MutableLiveData()
    fun getError(): LiveData<String> = _isErrorMessage

    private val _userList: MutableLiveData<List<UserListResponse>> = MutableLiveData()
    fun getUser(): LiveData<List<UserListResponse>> = _userList

    init {
        onTrigger()
    }

    private fun onTrigger() {
        _isLoading.value = true
        Coroutines.main {
            try {
                _userList.value = repository.getUsers()
                _isLoading.value = false
            } catch (e: ApiException) {
                _isLoading.value = false
                _isErrorMessage.value = e.message
            }
            catch (e: NoInternetException){
                _isLoading.value = false
                _isErrorMessage.value = e.message
            }
        }
    }


}