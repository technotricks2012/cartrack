package com.kishore.cartrack.ui.signup

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.kishore.cartrack.R
import com.kishore.cartrack.databinding.ActivitySignupBinding
import com.kishore.cartrack.utils.hide
import com.kishore.cartrack.utils.show
import com.kishore.cartrack.utils.snackbar
import kotlinx.android.synthetic.main.activity_signup.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class SignUpActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: SignUpViewModelFactory by instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)


        val binding: ActivitySignupBinding = DataBindingUtil.setContentView(this,R.layout.activity_signup)
        val viewModel = ViewModelProvider(this,factory).get(SignUpViewModel::class.java)
        binding.viewmodel = viewModel

        viewModel.isLoading().observe(this, {
            it?.let {
                when (it) {
                    true -> progressBar.show()
                    else -> progressBar.hide()
                }
            }
        })

        viewModel.getError().observe(this, Observer {
            rootLayout.snackbar(it,true)
        })

        viewModel.getSuccess().observe(this,{
            rootLayout.snackbar(it)
            finish()
        })

    }

    companion object {
        fun getStartIntent(
            context: Context
        ) = Intent(context, SignUpActivity::class.java)
    }
}