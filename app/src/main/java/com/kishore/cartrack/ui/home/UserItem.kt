package com.kishore.cartrack.ui.home

import com.kishore.cartrack.R
import com.kishore.cartrack.data.network.response.UserListResponse
import com.kishore.cartrack.databinding.ItemUserBinding
import com.xwray.groupie.databinding.BindableItem

class UserItem(val userListResponse: UserListResponse): BindableItem<ItemUserBinding>() {
    override fun getLayout(): Int = R.layout.item_user
    override fun bind(viewBinding: ItemUserBinding, position: Int) {
        viewBinding.users = userListResponse
    }

}