package com.kishore.cartrack.ui.signup

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kishore.cartrack.data.repositories.IAuthRepository


@Suppress("UNCHECKED_CAST")
class SignUpViewModelFactory(private val repository: IAuthRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SignUpViewModel(repository) as T
    }
}