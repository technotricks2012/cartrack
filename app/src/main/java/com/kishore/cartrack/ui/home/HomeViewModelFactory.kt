package com.kishore.cartrack.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kishore.cartrack.data.repositories.IUserRepository


@Suppress("UNCHECKED_CAST")
class HomeViewModelFactory(private val repository: IUserRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return HomeViewModel(repository) as T
    }
}